create table if not exists user_detail
(
    uuid uuid primary key unique not null,
    image uuid,
    status varchar(255),
    about_myself varchar(255)
);

create table if not exists users
(
    uuid uuid primary key unique not null,
    username varchar(45) unique not null,
    password varchar(45),
    detail_uuid uuid,
    active boolean,
    foreign key(detail_uuid) references user_detail (uuid)
);

insert into users (active, password, username, uuid) values (true, '123', 'admin', '25e8b837-55c3-43c8-a4c2-932f5afd75fd')