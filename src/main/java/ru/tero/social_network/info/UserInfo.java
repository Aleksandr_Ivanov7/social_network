package ru.tero.social_network.info;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.UUID;

@Entity
@Table(name = "users")
public class UserInfo {
    @Id
    @GeneratedValue
    private UUID uuid;
    @NotBlank(message = "Field username is empty")
    @Size(min = 3, max = 45, message = "Field username not valid size (3-45)")
    private String username;
    @NotBlank(message = "Field password is empty")
    private String password;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_detail_uuid")
    private UserDetailInfo userDetailInfo;
    private boolean active;

    public UserInfo() {
    }

    public UserInfo(String username, String password, boolean active) {
        this.username = username;
        this.password = password;
        this.active = active;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserDetailInfo getUserDetailInfo() {
        return userDetailInfo;
    }

    public void setUserDetailInfo(UserDetailInfo userDetailInfo) {
        this.userDetailInfo = userDetailInfo;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
