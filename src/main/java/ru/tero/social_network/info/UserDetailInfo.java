package ru.tero.social_network.info;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "user_detail")
public class UserDetailInfo {
    @Id
    @GeneratedValue
    private UUID uuid;
    private String image;
    private String status;
    @Column(name = "about_myself")
    private String aboutMyself;
    @OneToOne(mappedBy = "userDetailInfo", cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private UserInfo userInfo;

    public UserDetailInfo() {
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAboutMyself() {
        return aboutMyself;
    }

    public void setAboutMyself(String aboutMyself) {
        this.aboutMyself = aboutMyself;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
}
