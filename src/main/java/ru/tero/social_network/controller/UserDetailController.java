package ru.tero.social_network.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.tero.social_network.dao.UserDetailDao;
import ru.tero.social_network.info.UserDetailInfo;
import ru.tero.social_network.service.UserDetailService;

@Controller
@RequestMapping("/user_detail")
public class UserDetailController {

    @Autowired
    private UserDetailService userDetailService;

    @GetMapping("/form")
    public String getFormUserDetail(Model model){
        model.addAttribute("userDetailInfo", new UserDetailInfo());
        return "form_user_detail";
    }

    @PostMapping("/add")
    public String addUserDetail(@ModelAttribute("userDetailInfo") UserDetailInfo userDetailInfo){
        userDetailService.save(userDetailInfo);
        return "main";
    }

}
