package ru.tero.social_network.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.tero.social_network.authsession.AuthSession;
import ru.tero.social_network.info.UserInfo;
import ru.tero.social_network.service.UserService;

import javax.servlet.http.HttpSession;
import java.util.UUID;

@Controller
public class AuthController {

    private final static String AUTH_TOKEN= "auth-token";
    private String token;

    @Autowired
    private UserService service;

    @GetMapping("/enter")
    public String getFormLogin(Model model) {
        model.addAttribute("userInfo", new UserInfo());
        return "enter";
    }

    @PostMapping("/login")
    public String login(@ModelAttribute("userInfo") UserInfo user, Model model) {
        UserInfo userFromDB = service.get(user.getUsername(), user.getPassword());
        if (userFromDB != null) {
            model.addAttribute("user", userFromDB);
            token = UUID.randomUUID().toString();
//            AuthSession.add(userFromDB, token);
//            HttpHeaders headers = new HttpHeaders();
//            headers.add(AUTH_TOKEN, token);
            return "main";
        }
        return "registration";
    }

    @GetMapping("/logout")
    public String logout() {
//        String token = new HttpHeaders().getFirst(AUTH_TOKEN);
//        AuthSession.delete(AuthSession.currentUser(token));
        return "enter";
    }
}
