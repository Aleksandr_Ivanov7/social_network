package ru.tero.social_network.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.tero.social_network.info.UserInfo;
import ru.tero.social_network.service.UserService;

import javax.validation.Valid;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/new")
    public String getForm(Model model) {
        model.addAttribute("userInfo", new UserInfo());
        return "registration";
    }

    @PostMapping("/add")
    public String addUser(@Valid @ModelAttribute("userInfo") UserInfo user, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors())
            return "registration";
        userService.save(user);
        model.addAttribute("user", user);
        return "main";
    }

    @GetMapping("/{username}")
    public String getMainPage(@PathVariable("username") String username, Model model) {
        UserInfo user = userService.get(username);
        model.addAttribute("user", user);
        return "main";
    }
}
