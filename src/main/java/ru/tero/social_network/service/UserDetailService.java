package ru.tero.social_network.service;

import ru.tero.social_network.info.UserDetailInfo;

import java.util.UUID;

public interface UserDetailService {
    UserDetailInfo get(UUID uuid);
    void save(UserDetailInfo userDetail);

}
