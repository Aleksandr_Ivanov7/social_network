package ru.tero.social_network.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tero.social_network.dao.UserDetailDao;
import ru.tero.social_network.info.UserDetailInfo;

import java.util.UUID;

@Service
public class UserDetailServiceImpl implements UserDetailService{

    @Autowired
    private UserDetailDao userDetailDao;

    @Override
    @Transactional
    public UserDetailInfo get(UUID uuid) {
        return userDetailDao.get(uuid);
    }

    @Override
    @Transactional
    public void save(UserDetailInfo userDetail) {
        userDetailDao.save(userDetail);
    }
}
