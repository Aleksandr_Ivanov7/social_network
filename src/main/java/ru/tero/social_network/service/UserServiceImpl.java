package ru.tero.social_network.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tero.social_network.dao.UserDao;
import ru.tero.social_network.info.UserInfo;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserDao userDao;

    @Override
    @Transactional
    public UserInfo get(String username) {
        return userDao.get(username);
    }

    @Override
    @Transactional
    public UserInfo get(String username, String password) {
        return userDao.getUserByLoginPassword(username, password);
    }

    @Override
    @Transactional
    public void save(UserInfo userInfo) {
        userDao.save(userInfo);
    }

}
