package ru.tero.social_network.service;

import ru.tero.social_network.info.UserInfo;

public interface UserService {
    UserInfo get(String username);
    UserInfo get(String username, String password);
    void save(UserInfo userInfo);
}
