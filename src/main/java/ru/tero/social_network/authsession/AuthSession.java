package ru.tero.social_network.authsession;

import ru.tero.social_network.info.UserInfo;

import java.util.HashMap;
import java.util.Map;

public class AuthSession {
    private static Map<UserInfo, String> tokens;

    public static void add(UserInfo user, String token) {
        if (tokens == null) init();
        tokens.put(user, token);
    }

    public static boolean delete(UserInfo user) {
        if (tokens == null) return false;
        tokens.remove(user);
        return true;
    }

    private static void init() {
        tokens = new HashMap<>();
    }

    public static boolean isValid(String token) {
        if (tokens == null) return false;
        return tokens.containsValue(token);
    }

    public static UserInfo currentUser(String token) {
        for (Map.Entry entry : tokens.entrySet())
            if (entry.getValue().equals(token)) return (UserInfo) entry.getKey();
        return null;
    }
}
