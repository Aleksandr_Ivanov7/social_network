package ru.tero.social_network.dao;

import org.hibernate.HibernateException;
import org.hibernate.QueryException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.tero.social_network.info.UserInfo;

import javax.persistence.NoResultException;
import java.util.UUID;

@Repository
public class UserDao {

    @Autowired
    private SessionFactory factory;

    public UserDao() {
    }

    public UserInfo get(UUID uuid) {
        Session session;
        try {
            session = factory.getCurrentSession();
        } catch (HibernateException e) {
            session = factory.openSession();
        }
        return session.get(UserInfo.class, uuid);
    }

    public UserInfo get(String username) {
        Session session;
        try {
            session = factory.getCurrentSession();
        } catch (HibernateException e) {
            session = factory.openSession();
        }
        UserInfo userInfo = null;
        try {
            Query<UserInfo> query = session.createQuery("from UserInfo where username=:username", UserInfo.class);
            query.setParameter("username", username);
            userInfo = query.getSingleResult();
        } catch (QueryException | NoResultException ignored) {
        }
        return userInfo;
    }

    public void save(UserInfo user) {
        Session session;
        try {
            session = factory.getCurrentSession();
        } catch (HibernateException e) {
            e.printStackTrace();
            session = factory.openSession();
        }
        session.save(user);
    }

    public void update(){
        Session session;
        try {
            session = factory.getCurrentSession();
        } catch (HibernateException e) {
            session = factory.openSession();
        }
        session.createQuery("update UserInfo set ").executeUpdate();
    }

    public UserInfo getUserByLoginPassword(String username, String password) {
        Session session;
        try {
            session = factory.getCurrentSession();
        } catch (HibernateException e) {
            session = factory.openSession();
        }
        UserInfo userInfo;
        try {
            Query<UserInfo> query = session.createQuery("from UserInfo where username=:username and password=:password", UserInfo.class);
            query.setParameter("username", username);
            query.setParameter("password", password);
            userInfo = query.getSingleResult();
        } catch (QueryException | NoResultException e) {
            throw new NoResultException();
        }
        return userInfo;
    }
}
