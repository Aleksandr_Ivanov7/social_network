package ru.tero.social_network.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.tero.social_network.info.UserInfo;

import java.util.UUID;

@Repository
@Transactional
public class UserInfoDao {

    @Autowired
    private SessionFactory factory;

    public UserInfoDao() {
    }

    public UserInfo get(UUID uuid){
        Session session;
        try {
            session = factory.getCurrentSession();
        }catch (HibernateException e) {
            session = factory.openSession();
        }
        return session.get(UserInfo.class, uuid);
    }

    public void save(UserInfo user){
        Session session;
        try {
            session = factory.getCurrentSession();
        }catch (HibernateException e) {
            e.printStackTrace();
            session = factory.openSession();
        }
        session.save(user);
    }
}
